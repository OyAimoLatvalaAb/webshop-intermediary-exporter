# Webshop Intermediary Exporter

[![Build Status](https://jenkins.alatvala.fi/buildStatus/icon?job=Oy+Aimo+Latvala+Ab%2Fwebshop-intermediary-exporter%2Fmaster)](https://jenkins.alatvala.fi/job/Oy%20Aimo%20Latvala%20Ab/job/webshop-intermediary-exporter/job/master/)
[![Quality Gate Status](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=alert_status)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Maintainability Rating](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=sqale_rating)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Reliability Rating](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=reliability_rating)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Security Rating](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=security_rating)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Bugs](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=bugs)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Code Smells](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=code_smells)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Coverage](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=coverage)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Duplicated Lines (%)](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=duplicated_lines_density)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Lines of Code](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=ncloc)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Technical Debt](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=sqale_index)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)
[![Vulnerabilities](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-intermediary-exporter&metric=vulnerabilities)](https://sonar.alatvala.fi/dashboard?id=webshop-intermediary-exporter)

## What does this do?

This small utility queries the Webshop intermediary database for all its products, and then
inserts/updates them through `webshop-prod-info-backend` `CRUD API` to webshop prod info database.

## What has it eaten?

This project features tech such as

* TypeScript
* Winston
* dotenv
* MySQL
* Axios

So it is very basic.

## Usage

Copy over `/.example.env` -> `.env` and correct the database parameters, `API_BASE_URL` as well as the `JWT`
which `webshop-prod-info-backend` requires. And make sure you are running the `webshop-prod-info-backend` too!

After this, for development you may issue `yarn watch-server`, and for production you may run `yarn build` which outputs
compiled code into `/dist`, which then can be run by issuing `yarn start`.
