import CatalogVisibility from '../types/CatalogVisibility';

interface IProductInfo {
    EAN: string,
    salePrice: number | null,
    widthCm: number | null,
    heightCm: number | null,
    lengthCm: number | null,
    manufacturerWebUrl: string | null,
    unit: string | null,
    massG: number | null,
    text: string | null,
    catalogVisibility: CatalogVisibility | null,
    deferredDelivery: boolean | null,
}

interface IIntermediaryDatabaseProduct {
    EAN: string,
    NIMI: string | null,
    'T-RHM': string | null,
    HINTA: number | null,
    SALDO: number | null,
    SELITE: string | null,
    ALV: number | null,
    JULKAISIJA: string | null,
    'JULKAISIJAN-email': string | null,
    KUVAPOLKU: string | null,
    TOIMITUSTAPA: string | null,
    LEVEYS: number | null,
    PITUUS: number | null,
    KORKEUS: number | null,
    PAINO: number | null,
    YKSIKKO: string | null,
    VOLYYMI: number | null,
    ALAYKSIKKO: string | null,
    AKAYKS_per_YKSIKKO: number | null,
    VERTAILUHINTA: number | null,
    TOIMITTAJALINKKI: string | null,
    Alehinta: number | null,
    NAKYVYYS: CatalogVisibility | null,
    Vara1: string | null,
    Vara2: string | null,
    Vara3: string | null,
    Vara4: string | null,
    Vara5: string | null,
    Vara6: string | null,
    Vara7: string | null,
    Vara8: string | null,
}

export {
    IProductInfo,
    IIntermediaryDatabaseProduct,
};
