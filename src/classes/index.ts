import { IIntermediaryDatabaseProduct, IProductInfo } from '../interfaces';
import { CatalogVisibility } from '../types/CatalogVisibility';

class IntermediaryDatabaseProduct implements IIntermediaryDatabaseProduct {
    constructor(obj: IIntermediaryDatabaseProduct) {
        this.EAN = obj.EAN;
        this.NIMI = obj.NIMI;
        this['T-RHM'] = obj['T-RHM'];
        this.HINTA = obj.HINTA;
        this.SALDO = obj.SALDO;
        this.SELITE = obj.SELITE;
        this.ALV = obj.ALV;
        this.JULKAISIJA = obj.JULKAISIJA;
        this['JULKAISIJAN-email'] = obj['JULKAISIJAN-email'];
        this.KUVAPOLKU = obj.KUVAPOLKU;
        this.TOIMITUSTAPA = obj.TOIMITUSTAPA;
        this.LEVEYS = obj.LEVEYS;
        this.PITUUS = obj.PITUUS;
        this.KORKEUS = obj.KORKEUS;
        this.PAINO = obj.PAINO;
        this.YKSIKKO = obj.YKSIKKO;
        this.VOLYYMI = obj.VOLYYMI;
        this.ALAYKSIKKO = obj.ALAYKSIKKO;
        this.AKAYKS_per_YKSIKKO = obj.AKAYKS_per_YKSIKKO;
        this.VERTAILUHINTA = obj.VERTAILUHINTA;
        this.TOIMITTAJALINKKI = obj.TOIMITTAJALINKKI;
        this.Alehinta = obj.Alehinta;
        this.NAKYVYYS = obj.NAKYVYYS;
        this.Vara1 = obj.Vara1;
        this.Vara2 = obj.Vara2;
        this.Vara3 = obj.Vara3;
        this.Vara4 = obj.Vara4;
        this.Vara5 = obj.Vara5;
        this.Vara6 = obj.Vara6;
        this.Vara7 = obj.Vara7;
        this.Vara8 = obj.Vara8;
    }
    public readonly EAN: string;
    public readonly NIMI: string | null;
    public readonly 'T-RHM': string | null;
    public readonly HINTA: number | null;
    public readonly SALDO: number | null;
    public readonly SELITE: string | null;
    public readonly ALV: number | null;
    public readonly JULKAISIJA: string | null;
    public readonly 'JULKAISIJAN-email': string | null;
    public readonly KUVAPOLKU: string | null;
    public readonly TOIMITUSTAPA: string | null;
    public readonly LEVEYS: number | null;
    public readonly PITUUS: number | null;
    public readonly KORKEUS: number | null;
    public readonly PAINO: number | null;
    public readonly YKSIKKO: string | null;
    public readonly VOLYYMI: number | null;
    public readonly ALAYKSIKKO: string | null;
    public readonly AKAYKS_per_YKSIKKO: number | null;
    public readonly VERTAILUHINTA: number | null;
    public readonly TOIMITTAJALINKKI: string | null;
    public readonly Alehinta: number | null;
    public readonly NAKYVYYS: CatalogVisibility | null;
    public readonly Vara1: string | null;
    public readonly Vara2: string | null;
    public readonly Vara3: string | null;
    public readonly Vara4: string | null;
    public readonly Vara5: string | null;
    public readonly Vara6: string | null;
    public readonly Vara7: string | null;
    public readonly Vara8: string | null;
}

class ProductInfo implements IProductInfo {
    constructor(obj: IntermediaryDatabaseProduct) {
        this.EAN = obj.EAN;
        this.salePrice = obj.Alehinta;
        this.widthCm = obj.LEVEYS;
        this.heightCm = obj.KORKEUS;
        this.lengthCm = obj.PITUUS;
        this.manufacturerWebUrl = obj.TOIMITTAJALINKKI;
        this.unit = obj.YKSIKKO;
        this.massG = obj.PAINO;
        this.text = obj.SELITE;
        if(obj.NAKYVYYS != null && obj.NAKYVYYS.trim() != '') {
            this.catalogVisibility = obj.NAKYVYYS;
        } else {
            this.catalogVisibility = CatalogVisibility.visible;
        }
        this.deferredDelivery = false;
    }
    public readonly EAN: string;
    public readonly salePrice: number | null;
    public readonly widthCm: number | null;
    public readonly heightCm: number | null;
    public readonly lengthCm: number | null;
    public readonly manufacturerWebUrl: string | null;
    public readonly unit: string | null;
    public readonly massG: number | null;
    public readonly text: string | null;
    public readonly catalogVisibility: CatalogVisibility | null;
    public readonly deferredDelivery: boolean | null;
    public asApiPayload(): IProductInfo {
        return {
            EAN: this.EAN,
            salePrice: this.salePrice,
            widthCm: this.widthCm,
            heightCm: this.heightCm,
            lengthCm: this.lengthCm,
            manufacturerWebUrl: this.manufacturerWebUrl,
            unit: this.unit,
            massG: this.massG,
            text: this.text,
            catalogVisibility: this.catalogVisibility != null ? this.catalogVisibility : CatalogVisibility.visible,
            deferredDelivery: this.deferredDelivery,
        };
    }
}

export { IntermediaryDatabaseProduct, ProductInfo };
