import { Connection, createConnection, MysqlError } from 'mysql';
import * as winston from 'winston';
import {Logger} from 'winston';
import * as dotenv from 'dotenv';
import {IntermediaryDatabaseProduct, ProductInfo} from './classes';
import Axios, {AxiosResponse} from 'axios';
import {IIntermediaryDatabaseProduct} from './interfaces';

dotenv.config();

const logger: Logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.prettyPrint(),
        winston.format.splat(),
        winston.format.simple(),
    ),
    defaultMeta: { service: 'intermediary-exporter' },
    transports: [
        new winston.transports.Console()
    ]
});

logger.info('Application booting');

const connection: Connection = createConnection(
    `tcp://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
);

const requestOpts = {
    headers: {
        Authorization: `Bearer ${process.env.JWT}`,
    }
};

async function hasProductByEan(ean: string) {
    try {
        logger.info(`Searching for product with EAN ${ean}...`);
        const res = await Axios.get(`${process.env.API_BASE_URL}/product-controller/products/${ean}`,requestOpts);
        return res.status == 200;
    } catch(err) {
        if(err.response.status == 404) {
            return false;
        }
        logger.error(err.message);
        logger.error(JSON.stringify(err));
        throw err;
    }
}

async function insertProductInfo(product: ProductInfo) {
    const apiEndpoint = `${process.env.API_BASE_URL}/product-controller/products`;
    const res = await Axios.post(apiEndpoint, product.asApiPayload(), requestOpts);
    return res.status == 201;
}
async function updateProductInfo(product: ProductInfo) {
    const apiEndpoint = `${process.env.API_BASE_URL}/product-controller/products/${product.EAN}`;
    const res = await Axios.put(apiEndpoint, product.asApiPayload(), requestOpts);
    return res.status == 201;
}

class APIError extends Error {
    response!: AxiosResponse | null;
    constructor(message: string, response?: AxiosResponse) {
        super(message);
        Object.setPrototypeOf(this, new.target.prototype);
        this.name = APIError.name;
        typeof this.response == 'undefined' ? null : response;
    }
}

async function processRow(row: IIntermediaryDatabaseProduct): Promise<IntermediaryDatabaseProduct> {
    const intermediaryDatabaseProduct = new IntermediaryDatabaseProduct(row);
    const productInfo = new ProductInfo(intermediaryDatabaseProduct);

    const hasProductByEanRes = await hasProductByEan(productInfo.EAN);
    if(hasProductByEanRes) {
        logger.info(`Found product with ${productInfo.EAN}, updating...`);
        const updateProductRes = await updateProductInfo(productInfo);
        if(updateProductRes  == true) {
            logger.info(`Successfully updated product with EAN ${productInfo.EAN}`);
            return intermediaryDatabaseProduct;
        } else {
            throw new APIError(`Failed to insert product with EAN ${productInfo.EAN}`);
        }
    } else {
        logger.info(`Did not find product with ${productInfo.EAN}, inserting...`);
        const res = await insertProductInfo(productInfo);
        if(res == true) {
            logger.info(`Successfully inserted product with EAN ${productInfo.EAN}`);
            return intermediaryDatabaseProduct;
        }
        else {
            throw new APIError(`Failed to insert product with EAN ${productInfo.EAN}`);
        }
    }
}

const processBatches = async (logger: Logger, batches: Array<Array<IntermediaryDatabaseProduct>>) => {
    const failureAcc: Error[] = [];
    for(const [idx, batch] of batches.entries()) {
        logger.info(`Processing batch ${idx+1}/${batches.length}`);
        const promises: Array<Promise<IntermediaryDatabaseProduct>> = [];
        for(const row of batch) {
            let rowProcessingPromise: Promise<IntermediaryDatabaseProduct>;
            try {
                rowProcessingPromise = processRow(row);
            } catch(err) {
                rowProcessingPromise = Promise.reject(err.message);
            }
            promises.push(rowProcessingPromise);
        }
        logger.info(`Prepared batch ${idx+1}, waiting for it to complete...`);
        const results = await Promise.all(promises.map(p => p.catch(e => e)));
        const failures = results.filter(result => result instanceof Error);
        if(failures.length > 0) {
            // Something didn't work out
            for(const failure of failures) {
                failureAcc.push(failure);
                logger.error(`Error processing row ${failure}`);
                logger.error(JSON.stringify(failure));
            }
            logger.warn(`Done with batch ${idx+1}, with ${failures.length} errors (!)`);
        }
        logger.info(`Done with batch ${idx+1}/${batches.length}`);
    }
    if(failureAcc.length < 1) {
        logger.info('All batches processed successfully.');
    } else {
        logger.warn('All batches processed, with errors.');
        logger.error(JSON.stringify(failureAcc));
    }
};

(async () => {
    connection.connect();
    try {
        connection.query('SELECT * FROM product;', async (error: MysqlError | null, results: Array<IntermediaryDatabaseProduct>) => {
            connection.destroy();
            if(error !== null) {
                logger.error('Error querying the database');
                throw error;
            }
            // logger.error(error);
            const nRows = results.length;
            const batchMaxSize = 512;
            const batches: Array<Array<IntermediaryDatabaseProduct>> = new Array(Math.ceil(nRows / batchMaxSize)).fill(0).map(() => results.splice(0, batchMaxSize));
            await processBatches(logger, batches);
            logger.info('Done! Exiting successfully.');
            process.exit(0);
        });
    } catch (err) {
        logger.error(err);
        process.exit(1);
    }
})();
